Changelog
=========

v0.3
------

* Add support for window resizing and scrolling

v0.2
------

* Overhaul the UI and improve the UX

v0.1.2
------

* Implement functionality to add and subtract time

v0.1.1
------

* Change xterm-color to xterm to support curs_set(0)

v0.1
----

* Initial release
